// MAP INFORMATION
const map = [
    "████████",
    "███░░░██",
    "█OSB░░██",
    "███░BO██",
    "█O██B░██",
    "█░█░O░██",
    "█B░XBBO█",
    "█░░░O░░█",
    "████████"
];
// const map = [
//     "░░░░█████░░░░░░░░░░",
//     "░░░░█░░░█░░░░░░░░░░",
//     "░░░░█B░░█░░░░░░░░░░",
//     "░░███░░B██░░░░░░░░░",
//     "░░█░░B░B░█░░░░░░░░░",
//     "███░█░██░█░░░██████",
//     "█░░░█░██░█████░░OO█",
//     "█░B░░B░░░░░░░░░░OO█",
//     "█████░███░█S██░░OO█",
//     "░░░░█░░░░░█████████",
//     "░░░░███████░░░░░░░░"
// ]
// index 20 columns; index 14 rows
const wall = "█";
const walkway = "░";
const start = "S";
const targetForBox = "O";
const box = "B";
const targetWithBoxAlreadyThere = "X";
const mapHere = document.getElementById("mapHere");
const player = document.getElementById("player");
//80/rowcount15; cellcount for col


map.forEach((rowString, rowIndex) => {
    let rowDiv = document.createElement("div");
    rowDiv.classList.add("row");
    rowDiv.id = rowIndex;
    mapHere.appendChild(rowDiv);
    for(let cellCharacter of rowString) {
        const cellIndex = rowDiv.childElementCount;
        if(cellCharacter === wall) {
            rowDiv.appendChild(createCell("wall", cellIndex));
        } else if(cellCharacter === walkway) {
            rowDiv.appendChild(createCell("walkway", cellIndex));
        } else if(cellCharacter === start) {
            rowDiv.appendChild(createCell("start", cellIndex));
        } else if(cellCharacter === targetForBox) {
            rowDiv.appendChild(createCell("targetForBox", cellIndex));
        } else if(cellCharacter === box) {
            rowDiv.appendChild(createCell("box", cellIndex));
        } else if(cellCharacter === targetWithBoxAlreadyThere) {
            rowDiv.appendChild(createCell("targetWithBoxAlreadyThere", cellIndex));

        }
    }
});
function createCell(type, cellIndex) {
    let cellDiv = document.createElement("div");
    cellDiv.classList.add("cell", type);
    cellDiv.dataset.cell = cellIndex;
    if(type == "start") cellDiv.appendChild(player);
    if(type == "targetWithBoxAlreadyThere") {
        cellDiv.classList.add("targetForBox");
        cellDiv.classList.add("box");
    }
    return cellDiv;
}



// PLAYER INFORMATION
function moveLogicForUpAndDown() {
    const currentCell = player.parentElement;
    const currentRow = currentCell.parentElement;
    let newRowId = Number(currentRow.id) + moveRowIndexBy;
    let newRow = document.getElementById(newRowId);
    let newRowBoxId = newRowId + moveRowIndexBy;
    let newRowBox = document.getElementById(newRowBoxId);
    let cellCount = currentCell.dataset.cell;
    let destinationCell = newRow.childNodes[cellCount];
    let boxDestinationCell = newRowBox.childNodes[cellCount];
    checkDestinationCellInformation(destinationCell, boxDestinationCell);
}
function pressDownArrow() {
    moveRowIndexBy = 1;
    moveLogicForUpAndDown();
    player.style.transform = "scaleY(1)";
    checkForWin();
}
function pressUpArrow() {
    moveRowIndexBy = -1;
    moveLogicForUpAndDown(); 
    player.style.transform = "scaleY(-1)";
    checkForWin();
}

function pressLeftArrow() {
    let destinationCell = player.parentElement.previousSibling;
    let boxDestinationCell = player.parentElement.previousSibling.previousSibling;
    player.style.transform = "scaleX(1)";
    checkDestinationCellInformation(destinationCell, boxDestinationCell);
    checkForWin();
}
function pressRightArrow() {
    let destinationCell = player.parentElement.nextSibling;
    let boxDestinationCell = player.parentElement.nextSibling.nextSibling;
    player.style.transform = "scaleX(-1)";
    checkDestinationCellInformation(destinationCell, boxDestinationCell);
    checkForWin();
}

function checkDestinationCellInformation(destinationCell, boxDestinationCell) {
    if(checkForBox(destinationCell) && !checkForBox(boxDestinationCell) && !checkForWall(boxDestinationCell)) {
        destinationCell.classList.remove("box");
        boxDestinationCell.classList.add("box");
        destinationCell.appendChild(player);
    }
    if(!checkForBox(destinationCell) && !checkForWall(destinationCell)) {
        destinationCell.appendChild(player);
    }
}
function checkForBox(destinationCell) {
    if(destinationCell.classList.contains("box")) return true;
}
function checkForWall(destinationCell) {
    if(destinationCell.classList.contains("wall")) return true;
}

function checkForWin() {
    let filledTargets = document.querySelectorAll(".targetForBox.box");
    let filledTargetsLength = filledTargets.length;
    let numberOfBoxes = document.querySelectorAll(".box")
    if(filledTargetsLength == numberOfBoxes.length) {
        winningMessage()
    }
}
function winningMessage() {
    const winningMessageHere = document.getElementById("mapHere");
    let messageForWinner = document.createElement("h1");
    messageForWinner.textContent = "GOOD JOB, MASTER OF THE FLOWERS!";
    messageForWinner.classList.add("messageStyle");
    winningMessageHere.appendChild(messageForWinner).style;
}


document.addEventListener('keydown', (event) => {
    const keyName = event.key;
    switch (keyName) {
        case "ArrowDown":
            pressDownArrow();
            changeImage();
            break;
        case "ArrowUp":
            pressUpArrow();
            changeImage();
            break;
        case "ArrowLeft":
            pressLeftArrow();
            changeImage();
            break;
        case "ArrowRight":
            pressRightArrow();
            changeImage();
            break;
    }
});
document.addEventListener('keyup', (event) => {
    const keyName = event.key;
    switch (keyName) {
        case "ArrowDown":
        case "ArrowUp":
        case "ArrowLeft":
        case "ArrowRight":
            changeImageBack()
            break;
    }
});
function changeImage() {
    document.getElementById("player").classList.add("ladyKick");
}
function changeImageBack() {
    document.getElementById("player").classList.remove("ladyKick") ;
}


function resetPlayerToStart() {
    location.reload();
}